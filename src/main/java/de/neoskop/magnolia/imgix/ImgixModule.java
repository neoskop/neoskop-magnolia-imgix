package de.neoskop.magnolia.imgix;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.neoskop.magnolia.imgix.model.NgrokTunnel;
import de.neoskop.magnolia.imgix.model.NgrokTunnelList;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImgixModule implements ModuleLifecycle {
  public static final String IMGIX_SETTINGS_SUFFIX = "ImgixSettings";
  private static final String SERVER_URL_ENV = "SERVER_URL";
  private static final String NGROK_ENABLED_ENV = "NGROK_ENABLED";
  private static final String NGROK_URL_ENV = "NGROK_URL";
  private static final String NGROK_DEFAULT_URL = "http://ngrok:4040/api/tunnels";
  private static final Logger LOG = LogManager.getLogger(ImgixModule.class);
  private String apiKey = "KQuK6pvaZNRrJGM7EdgD6uNSSAEm5N4Y";
  private String signKey;
  private String baseUrl;
  private String cacheServerUrl = "";
  private String serverUrl;
  private String ngrokServerUrl;
  private Map<String, String> defaultParams = new HashMap<>();

  public void start(ModuleLifecycleContext ctx) {
    if (StringUtils.isNotBlank(System.getenv(NGROK_ENABLED_ENV))
        && System.getenv(NGROK_ENABLED_ENV).equalsIgnoreCase("true")) {
      try {
        ngrokServerUrl = this.determineNgrokUrl();
      } catch (IOException e) {
        LOG.error("Determining Ngrok URL failed", e);
      }
    }
  }

  public void stop(ModuleLifecycleContext ctx) {}

  private String determineNgrokUrl() throws IOException {
    final String url =
        StringUtils.isNotBlank(System.getenv(NGROK_URL_ENV))
            ? System.getenv(NGROK_URL_ENV)
            : NGROK_DEFAULT_URL;
    final Gson gson = new GsonBuilder().create();
    final Content content =
        Request.Get(url).setHeader("Content-Type", "application/json").execute().returnContent();
    final NgrokTunnelList tunnels = gson.fromJson(content.asString(), NgrokTunnelList.class);
    return tunnels
        .getTunnels()
        .stream()
        .filter(t -> t.getProto().equalsIgnoreCase("https"))
        .findFirst()
        .map(NgrokTunnel::getPublicUrl)
        .orElse(null);
  }

  public String getServerUrl() {
    if (StringUtils.isNotBlank(ngrokServerUrl)) {
      return ngrokServerUrl;
    }

    if (StringUtils.isNotBlank(serverUrl)) {
      return serverUrl;
    }

    if (StringUtils.isNotBlank(System.getenv(SERVER_URL_ENV))) {
      return System.getenv(SERVER_URL_ENV);
    }

    if (MgnlContext.isWebContext()) {
      final HttpServletRequest request = MgnlContext.getWebContext().getRequest();
      String hostName;

      if (request.getHeader("X-Forwarded-Host") != null) {
        hostName = request.getHeader("X-Forwarded-Host");
      } else {
        hostName =
            request.getServerName()
                + (request.getServerPort() != 80 && request.getServerPort() != 443
                    ? ":" + request.getServerPort()
                    : "");
      }

      cacheServerUrl = (request.isSecure() ? "https://" : "http://") + hostName;
    }

    return cacheServerUrl;
  }

  public void setServerUrl(String serverUrl) {
    this.serverUrl = serverUrl;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getSignKey() {
    return signKey;
  }

  public void setSignKey(String signKey) {
    this.signKey = signKey;
  }

  public Map<String, String> getDefaultParams() {
    return defaultParams;
  }

  public void setDefaultParams(Map<String, String> defaultParams) {
    this.defaultParams = defaultParams;
  }
}
