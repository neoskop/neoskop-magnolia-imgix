package de.neoskop.magnolia.imgix.functions;

import com.imgix.URLBuilder;
import de.neoskop.magnolia.imgix.ImgixModule;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.NodeUtil;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 25.10.16
 */
@Singleton
public class ImgixTemplatingFunctions {

  private static final Logger LOG = LogManager.getLogger(ImgixTemplatingFunctions.class);
  private final DamTemplatingFunctions damfn;
  private final ImgixModule module;

  @Inject
  public ImgixTemplatingFunctions(DamTemplatingFunctions damfn, ImgixModule module) {
    this.damfn = damfn;
    this.module = module;
  }

  public String getProxyUrl(String uri) {
    return getProxyUrl(uri, "");
  }

  public String getProxyUrl(String uri, String options) {
    Map<String, String> params = getDefaultParams();
    Stream.of(options.split("&"))
        .forEach(
            o -> {
              final String[] parts = o.split("=");

              if (parts.length > 1) {
                params.put(parts[0], parts[1]);
              }
            });

    final URLBuilder builder = getUrlBuilder();
    String url = module.getServerUrl() + uri;

    try {
      url = URLEncoder.encode(url, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      LOG.warn("Could not encode asset path", e);
    }

    return builder.createURL(url, params);
  }

  public String getAssetUrl(ContentMap content, String propertyName) {
    final Asset asset = damfn.getAsset((String) content.get(propertyName));

    if (asset == null) {
      return "";
    }

    Map<String, String> params = getDefaultParams();
    fillParamsFromNode(content, propertyName, params);
    return getUrl(asset, params);
  }

  private Map<String, String> getDefaultParams() {
    Map<String, String> params = new HashMap<>();
    params.put("auto", "format,enhance,compress");
    params.putAll(module.getDefaultParams());
    return params;
  }

  public String getAssetUrl(ContentMap content, String propertyName, String options) {
    final Asset asset = damfn.getAsset((String) content.get(propertyName));

    if (asset == null) {
      return "";
    }

    final Map<String, String> params = getDefaultParams();
    Stream.of(options.split("&"))
        .forEach(
            o -> {
              final String[] parts = o.split("=");

              if (parts.length > 1) {
                params.put(parts[0], parts[1]);
              }
            });
    fillParamsFromNode(content, propertyName, params);

    if (params.containsKey("w") && params.containsKey("h")) {
      params.putIfAbsent("fit", "crop");
      params.putIfAbsent("crop", "entropy");
    }

    return getUrl(asset, params);
  }

  private void fillParamsFromNode(
      ContentMap content, String propertyName, Map<String, String> params) {
    final Node node = content.getJCRNode();
    try {
      final String settingsNodeName = propertyName + "ImgixSettings";

      if (node.hasNode(settingsNodeName) && params.containsKey("w") && params.containsKey("h")) {
        final Node settingsNode = node.getNode(settingsNodeName);
        params.put("crop", "focalpoint");
        params.put("fit", "crop");
        params.put("fp-x", settingsNode.getProperty("fpX").getString());
        params.put("fp-y", settingsNode.getProperty("fpY").getString());
      }
    } catch (RepositoryException e) {
      LOG.warn("Could not retrieve crop type for " + NodeUtil.getPathIfPossible(node), e);
    }
  }

  private String getUrl(Asset asset, Map<String, String> params) {
    final String path = asset.getLink();
    final URLBuilder builder = getUrlBuilder();
    String url = module.getServerUrl() + path;

    try {
      url = URLEncoder.encode(url, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      LOG.warn("Could not encode asset path", e);
    }

    return builder.createURL(url, params);
  }

  private URLBuilder getUrlBuilder() {
    final URLBuilder builder = new URLBuilder(module.getBaseUrl());
    builder.setUseHttps(true);
    builder.setSignKey(module.getSignKey());
    return builder;
  }
}
