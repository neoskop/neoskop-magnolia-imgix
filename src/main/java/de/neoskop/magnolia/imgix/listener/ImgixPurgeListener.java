package de.neoskop.magnolia.imgix.listener;

import com.google.common.collect.Sets;
import de.neoskop.magnolia.imgix.ImgixModule;
import info.magnolia.context.MgnlContext;
import info.magnolia.dam.core.config.DamCoreConfiguration;
import info.magnolia.dam.jcr.AssetNodeTypes;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 01.12.16
 */
public class ImgixPurgeListener implements EventListener {

  private static final Logger LOG = LogManager.getLogger(ImgixPurgeListener.class);
  private static final String PURGER_ENDPOINT = "https://api.imgix.com/v2/image/purger";
  private static final Set<String> IMAGE_EXTENSIONS =
      Sets.newHashSet("jpg", "jpeg", "png", "gif", "tiff", "bmp", "icns", "ico", "psd", "ai");

  @Override
  public void onEvent(EventIterator events) {
    final List<String> processedUUIDs = new ArrayList<>();

    MgnlContext.doInSystemContext(
        () -> {
          try {
            while (events.hasNext()) {
              final Event event = events.nextEvent();

              if (!processedUUIDs.contains(event.getIdentifier())) {
                final Node node =
                    MgnlContext.getJCRSession(DamConstants.WORKSPACE)
                        .getNodeByIdentifier(event.getIdentifier());

                if (node != null && node.isNodeType(AssetNodeTypes.Asset.NAME)) {
                  final String fileName =
                      PropertyUtil.getString(
                          AssetNodeTypes.AssetResource.getResourceNodeFromAsset(node),
                          AssetNodeTypes.AssetResource.FILENAME,
                          "");

                  if (IMAGE_EXTENSIONS.stream().map(e -> "." + e).anyMatch(fileName::endsWith)) {
                    final String purgePath =
                        Components.getComponent(DamCoreConfiguration.class).getDownloadPath()
                            + event.getIdentifier()
                            + "/"
                            + fileName;
                    final String purgeUrl =
                        Components.getComponent(ImgixModule.class).getServerUrl() + purgePath;
                    purgeImgixUrl(purgeUrl);
                    LOG.debug("Purged " + purgeUrl);
                  }
                }

                processedUUIDs.add(event.getIdentifier());

                if (processedUUIDs.size() > 1000) {
                  processedUUIDs.remove(0);
                }
              }
            }
          } catch (RepositoryException e) {
            LOG.error("Could not check node for changes", e);
          } catch (IOException e) {
            LOG.error("Purging of URL failed", e);
          }

          return true;
        },
        true);
  }

  private void purgeImgixUrl(String purgeUrl) throws IOException {
    final String apiKey = Components.getComponent(ImgixModule.class).getApiKey();
    final BasicHeader authorizationHeader =
        new BasicHeader("Authorization", Base64.encodeBase64String((apiKey + ":").getBytes()));
    Request.Post(PURGER_ENDPOINT)
        .addHeader(authorizationHeader)
        .bodyForm(Form.form().add("url", purgeUrl).build())
        .execute()
        .handleResponse(
            r -> {
              final StatusLine statusLine = r.getStatusLine();
              if (statusLine.getStatusCode() != 410) {
                throw new HttpResponseException(
                    statusLine.getStatusCode(), statusLine.getReasonPhrase());
              }

              return null;
            });
  }
}
