package de.neoskop.magnolia.imgix.fieldBuilders;

import de.neoskop.magnolia.imgix.definition.ImgixAssetFieldDefinition;
import info.magnolia.ui.form.config.AbstractFieldBuilder;
import info.magnolia.ui.form.config.GenericValidatorBuilder;
import info.magnolia.ui.form.field.transformer.Transformer;
import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/** Builder for building a imgix asset field definition. */
public class ImgixAssetFieldBuilder extends AbstractFieldBuilder {

  private final ImgixAssetFieldDefinition definition = new ImgixAssetFieldDefinition();

  public ImgixAssetFieldBuilder(String name) {
    this.definition().setName(name);
  }

  @Override
  public ImgixAssetFieldDefinition definition() {
    return definition;
  }

  @Override
  public ImgixAssetFieldBuilder label(String label) {
    return (ImgixAssetFieldBuilder) super.label(label);
  }

  @Override
  public ImgixAssetFieldBuilder i18nBasename(String i18nBasename) {
    return (ImgixAssetFieldBuilder) super.i18nBasename(i18nBasename);
  }

  @Override
  public ImgixAssetFieldBuilder i18n(boolean i18n) {
    return (ImgixAssetFieldBuilder) super.i18n(i18n);
  }

  @Override
  public ImgixAssetFieldBuilder i18n() {
    return (ImgixAssetFieldBuilder) super.i18n();
  }

  @Override
  public ImgixAssetFieldBuilder description(String description) {
    return (ImgixAssetFieldBuilder) super.description(description);
  }

  @Override
  public ImgixAssetFieldBuilder type(String type) {
    return (ImgixAssetFieldBuilder) super.type(type);
  }

  @Override
  public ImgixAssetFieldBuilder required(boolean required) {
    return (ImgixAssetFieldBuilder) super.required(required);
  }

  @Override
  public ImgixAssetFieldBuilder required() {
    return (ImgixAssetFieldBuilder) super.required();
  }

  @Override
  public ImgixAssetFieldBuilder requiredErrorMessage(String requiredErrorMessage) {
    return (ImgixAssetFieldBuilder) super.requiredErrorMessage(requiredErrorMessage);
  }

  @Override
  public ImgixAssetFieldBuilder readOnly(boolean readOnly) {
    return (ImgixAssetFieldBuilder) super.readOnly(readOnly);
  }

  @Override
  public ImgixAssetFieldBuilder readOnly() {
    return (ImgixAssetFieldBuilder) super.readOnly();
  }

  @Override
  public ImgixAssetFieldBuilder defaultValue(String defaultValue) {
    return (ImgixAssetFieldBuilder) super.defaultValue(defaultValue);
  }

  @Override
  public ImgixAssetFieldBuilder styleName(String styleName) {
    return (ImgixAssetFieldBuilder) super.styleName(styleName);
  }

  @Override
  public ImgixAssetFieldBuilder validator(ConfiguredFieldValidatorDefinition validatorDefinition) {
    return (ImgixAssetFieldBuilder) super.validator(validatorDefinition);
  }

  @Override
  public ImgixAssetFieldBuilder validator(GenericValidatorBuilder validatorBuilder) {
    return (ImgixAssetFieldBuilder) super.validator(validatorBuilder);
  }

  @Override
  public ImgixAssetFieldBuilder transformerClass(Class<? extends Transformer<?>> transformerClass) {
    return (ImgixAssetFieldBuilder) super.transformerClass(transformerClass);
  }
}
