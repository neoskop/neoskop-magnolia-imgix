package de.neoskop.magnolia.imgix.definition;

import de.neoskop.magnolia.imgix.transformer.ImgixAssetTransformer;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;

/**
 * @author Arne Diekmann
 * @since 28.02.17
 */
public class ImgixAssetFieldDefinition extends ConfiguredFieldDefinition {

  public ImgixAssetFieldDefinition() {
    setTransformerClass(ImgixAssetTransformer.class);
  }
}
