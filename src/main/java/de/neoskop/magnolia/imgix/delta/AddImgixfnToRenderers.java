package de.neoskop.magnolia.imgix.delta;

import static info.magnolia.repository.RepositoryConstants.CONFIG;

import de.neoskop.magnolia.imgix.functions.ImgixTemplatingFunctions;
import info.magnolia.jcr.util.NodeTypes.ContentNode;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.AbstractRepositoryTask;
import info.magnolia.module.delta.TaskExecutionException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

/**
 * @author Arne Diekmann
 * @since 07.12.16
 */
public class AddImgixfnToRenderers extends AbstractRepositoryTask {

  public static final String IMGIXFN = "imgixfn";
  private static final String CONTEXT_ATTRIBUTES = "contextAttributes";

  public AddImgixfnToRenderers() {
    super("Add imgixfn to renderers", "Add imgixfn to all available renderers");
  }

  @Override
  protected void doExecute(InstallContext ctx) throws RepositoryException, TaskExecutionException {
    final NodeIterator nodeIt =
        ctx.getJCRSession(CONFIG).getNode("/modules/rendering/renderers").getNodes();

    while (nodeIt.hasNext()) {
      final Node rendererNode = nodeIt.nextNode();

      if (!rendererNode.hasNode(CONTEXT_ATTRIBUTES)) {
        rendererNode.addNode(CONTEXT_ATTRIBUTES, ContentNode.NAME);
      }

      final Node ctxAttrsNode = rendererNode.getNode(CONTEXT_ATTRIBUTES);

      if (!ctxAttrsNode.hasNode(IMGIXFN)) {
        ctxAttrsNode.addNode(IMGIXFN, ContentNode.NAME);
      }

      final Node imgixFnNode = ctxAttrsNode.getNode(IMGIXFN);
      imgixFnNode.setProperty("componentClass", ImgixTemplatingFunctions.class.getName());
      imgixFnNode.setProperty("name", IMGIXFN);
      rendererNode.getSession().save();
    }
  }
}
