package de.neoskop.magnolia.imgix.setup;

import de.neoskop.magnolia.imgix.delta.AddImgixfnToRenderers;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleModuleResource;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.Task;
import java.util.Collections;
import java.util.List;

public class ImgixModuleVersionHandler extends DefaultModuleVersionHandler {

  public ImgixModuleVersionHandler() {
    register(
        DeltaBuilder.update("1.0.8", "")
            .addTask(new BootstrapSingleModuleResource("config.modules.imgix.fieldTypes.xml")));
  }

  @Override
  protected List<Task> getExtraInstallTasks(InstallContext installContext) {
    return Collections.singletonList(new AddImgixfnToRenderers());
  }
}
