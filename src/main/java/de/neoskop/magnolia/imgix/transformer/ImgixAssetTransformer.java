package de.neoskop.magnolia.imgix.transformer;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.util.ObjectProperty;
import de.neoskop.magnolia.imgix.ImgixModule;
import de.neoskop.magnolia.imgix.model.ImgixAsset;
import de.neoskop.magnolia.imgix.model.ImgixFocalPoint;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.form.field.definition.ConfiguredFieldDefinition;
import info.magnolia.ui.form.field.transformer.basic.BasicTransformer;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 28.02.17
 */
public class ImgixAssetTransformer extends BasicTransformer<ImgixAsset> {

  private static final Logger LOG = LogManager.getLogger(ImgixAssetTransformer.class);

  public ImgixAssetTransformer(
      Item relatedFormItem, ConfiguredFieldDefinition definition, Class<ImgixAsset> type) {
    super(relatedFormItem, definition, type, null);
  }

  @Override
  public ImgixAsset readFromItem() {
    final Property<String> imageProperty = getOrCreateProperty(definition.getName());

    if (StringUtils.isNotBlank(imageProperty.getValue())) {
      final ImgixAsset imgixAsset = new ImgixAsset(imageProperty.getValue());
      final JcrNodeAdapter rootNode = (JcrNodeAdapter) relatedFormItem;

      try {
        final String childNodeName = definition.getName() + ImgixModule.IMGIX_SETTINGS_SUFFIX;

        if (rootNode.getJcrItem().hasNode(childNodeName)) {
          final Node childNode = rootNode.getJcrItem().getNode(childNodeName);
          final String focalPointX = PropertyUtil.getString(childNode, "fpX");
          final String focalPointY = PropertyUtil.getString(childNode, "fpY");
          imgixAsset.setFocalPoint(new ImgixFocalPoint(focalPointX, focalPointY));
        }
      } catch (RepositoryException e) {
        LOG.error("Could not resolve imgix settings", e);
      }

      return imgixAsset;
    }

    final ImgixAsset newImgixAsset = new ImgixAsset();
    imageProperty.setValue("");
    return newImgixAsset;
  }

  @Override
  public void writeToItem(ImgixAsset newValue) {
    getOrCreateProperty(definition.getName()).setValue(newValue.getIdentifier());
    final JcrNodeAdapter rootNode = (JcrNodeAdapter) relatedFormItem;
    final String childNodeName = definition.getName() + ImgixModule.IMGIX_SETTINGS_SUFFIX;

    if (newValue.getFocalPoint() != null) {
      try {
        if (rootNode.getJcrItem().hasNode(childNodeName)) {
          rootNode.getJcrItem().getNode(childNodeName).remove();
          rootNode.getJcrItem().getSession().save();
        }

        JcrNodeAdapter childNode =
            new JcrNewNodeAdapter(rootNode.getJcrItem(), NodeTypes.ContentNode.NAME, childNodeName);

        rootNode.addChild(childNode);
        childNode.getChildren().clear();
        childNode.addItemProperty(
            "fpX", new ObjectProperty<>(newValue.getFocalPoint().getX().replace(",", ".")));
        childNode.addItemProperty(
            "fpY", new ObjectProperty<>(newValue.getFocalPoint().getY().replace(",", ".")));
      } catch (RepositoryException e) {
        LOG.error("", e);
      }
    }
  }

  private Property<String> getOrCreateProperty(String name) {
    Property<String> property = relatedFormItem.getItemProperty(name);

    if (property == null) {
      property = new ObjectProperty<>("");
      relatedFormItem.addItemProperty(definition.getName(), property);
    }

    return property;
  }
}
