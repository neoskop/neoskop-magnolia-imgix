package de.neoskop.magnolia.imgix.model;

import com.google.gson.annotations.SerializedName;

public class NgrokTunnel {
  private String proto;

  @SerializedName("public_url")
  private String publicUrl;

  public String getProto() {
    return proto;
  }

  public void setProto(String proto) {
    this.proto = proto;
  }

  public String getPublicUrl() {
    return publicUrl;
  }

  public void setPublicUrl(String publicUrl) {
    this.publicUrl = publicUrl;
  }
}
