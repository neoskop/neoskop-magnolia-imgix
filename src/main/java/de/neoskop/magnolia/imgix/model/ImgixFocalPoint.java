package de.neoskop.magnolia.imgix.model;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author Arne Diekmann
 * @since 01.03.17
 */
public class ImgixFocalPoint {

  private String x;
  private String y;

  public ImgixFocalPoint(double x, double y) {
    this.x = roundDouble(x);
    this.y = roundDouble(y);
  }

  public ImgixFocalPoint(String x, String y) {
    this.x = x;
    this.y = y;
  }

  public String getX() {
    return x;
  }

  public String getY() {
    return y;
  }

  private String roundDouble(double value) {
    DecimalFormat df = new DecimalFormat("#.##");
    df.setRoundingMode(RoundingMode.CEILING);
    return df.format(value);
  }
}
