package de.neoskop.magnolia.imgix.model;

import java.util.List;

public class NgrokTunnelList {
  private List<NgrokTunnel> tunnels;

  public List<NgrokTunnel> getTunnels() {
    return tunnels;
  }

  public void setTunnels(List<NgrokTunnel> tunnels) {
    this.tunnels = tunnels;
  }
}
