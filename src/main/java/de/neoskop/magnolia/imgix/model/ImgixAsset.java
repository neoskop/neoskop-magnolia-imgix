package de.neoskop.magnolia.imgix.model;

/**
 * @author Arne Diekmann
 * @since 01.03.17
 */
public class ImgixAsset {

  private String identifier;
  private ImgixFocalPoint focalPoint;

  public ImgixAsset() {}

  public ImgixAsset(String identifier) {
    this.identifier = identifier;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public ImgixFocalPoint getFocalPoint() {
    return focalPoint;
  }

  public void setFocalPoint(ImgixFocalPoint focalPoint) {
    this.focalPoint = focalPoint;
  }
}
