package de.neoskop.magnolia.imgix.field;

import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.Layout;
import com.vaadin.ui.NativeButton;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.data.Property.ReadOnlyException;
import com.vaadin.v7.data.util.converter.Converter;
import com.vaadin.v7.shared.ui.label.ContentMode;
import com.vaadin.v7.ui.CustomField;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.TextField;
import com.vaadin.v7.ui.VerticalLayout;
import de.neoskop.magnolia.imgix.model.ImgixAsset;
import de.neoskop.magnolia.imgix.model.ImgixFocalPoint;
import info.magnolia.cms.core.FileSystemHelper;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.AssetProvider;
import info.magnolia.dam.api.AssetProviderRegistry;
import info.magnolia.dam.api.Item;
import info.magnolia.dam.app.assets.field.translator.AssetCompositeIdKeyTranslator;
import info.magnolia.dam.app.ui.field.configuration.image.ImageThumbnailComponentProvider;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.dam.jcr.JcrAssetProvider;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.vaadin.integration.NullItem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 28.02.17
 */
public class ImgixAssetField extends CustomField<ImgixAsset> {

  private static final Logger LOG = LogManager.getLogger(ImgixAssetField.class);
  private final VerticalLayout rootLayout = new VerticalLayout();
  private final HorizontalLayout linkLayout = new HorizontalLayout();
  private final TextField textField = new TextField();
  private final Button selectButton = new NativeButton();
  private final Layout previewLayout = new VerticalLayout();
  private final String id;
  private Asset currentAsset;
  private String currentMimeType;
  private File currentAssetFile;
  private Label focalPointLabel;
  private ImgixAsset currentValue = new ImgixAsset();
  private AssetCompositeIdKeyTranslator converter = new AssetCompositeIdKeyTranslator();

  public ImgixAssetField(String name) {
    id = name + "-imgix-asset-field";
    converter.setWorkspaceName("dam");
  }

  @Override
  protected Component initContent() {
    Page.Styles styles = Page.getCurrent().getStyles();
    styles.add(".imgix-asset-preview { background-color: #d1d1d1; text-align: center; }");

    JavaScript.getCurrent().removeFunction("getActualImageWidth");
    JavaScript.getCurrent()
        .addFunction(
            "getActualImageWidth",
            (JavaScriptFunction)
                arguments -> {
                  double relativeX = arguments.getNumber(0);
                  double relativeY = arguments.getNumber(1);
                  double currentClientWidth = arguments.getNumber(2);
                  double currentClientHeight = arguments.getNumber(3);
                  double focalPointX = relativeX / currentClientWidth;
                  double focalPointY = relativeY / currentClientHeight;
                  currentValue.setFocalPoint(new ImgixFocalPoint(focalPointX, focalPointY));
                  getPropertyDataSource().setValue(currentValue);
                  updateFields();
                });

    addStyleName("linkfield");
    rootLayout.setSizeFull();
    setupPreviewLayout();
    setupTextField();
    setupSelectButton();
    setupLinkLayout();
    rootLayout.addComponent(previewLayout);
    rootLayout.addComponent(linkLayout);
    return rootLayout;
  }

  private void setupPreviewLayout() {
    previewLayout.addStyleName("imgix-asset-preview");
  }

  private void setupTextField() {
    textField.setImmediate(true);
    textField.setWidth(100, Unit.PERCENTAGE);
    textField.setNullRepresentation("");
    textField.setNullSettingAllowed(true);
    textField.setConverter(AssetCompositeIdKeyTranslator.class);
    textField.addValueChangeListener(this::refreshPreview);
  }

  private void refreshPreview(Property.ValueChangeEvent event) {
    String itemReference = event.getProperty().getValue().toString();
    Asset item = null;
    currentValue.setIdentifier(converter.convertToModel(itemReference, String.class, getLocale()));
    getPropertyDataSource().setValue(currentValue);

    if (StringUtils.isNotBlank(itemReference)) {
      item = refreshItem(itemReference);
    }

    if (item != null && !(item instanceof NullItem)) {
      final ImageThumbnailComponentProvider thumbnailProvider =
          Components.getComponentProvider().newInstance(ImageThumbnailComponentProvider.class);
      final Image image =
          (Image)
              thumbnailProvider.createThumbnailComponent(null, currentAssetFile, currentMimeType);
      image.setId(id);
      image.addStyleName("preview-image");
      image.addClickListener(
          e ->
              JavaScript.getCurrent()
                  .execute(
                      "getActualImageWidth("
                          + e.getRelativeX()
                          + ", "
                          + e.getRelativeY()
                          + ", document.getElementById('"
                          + image.getId()
                          + "').clientWidth, document.getElementById('"
                          + image.getId()
                          + "').clientHeight);"));
      previewLayout.removeAllComponents();
      previewLayout.addComponent(image);
      focalPointLabel = new Label("<strong>Focal-Point:</strong> -", ContentMode.HTML);
      previewLayout.addComponent(focalPointLabel);
      previewLayout.setVisible(true);
    } else {
      previewLayout.setVisible(false);
    }
  }

  private Asset refreshItem(String itemkey) {
    AssetProvider provider =
        Components.getComponent(AssetProviderRegistry.class)
            .getProviderById(DamConstants.DEFAULT_JCR_PROVIDER_ID);
    String relativeAssetPath =
        StringUtils.removeStart(itemkey, ((JcrAssetProvider) provider).getRootPath());

    Item item = ((JcrAssetProvider) provider).getItem(relativeAssetPath);
    if (item == null || !item.isAsset()) {
      return null;
    }

    currentAsset = (Asset) item;
    currentMimeType = currentAsset.getMimeType();

    if (currentMimeType.isEmpty()) {
      return null;
    }

    currentAssetFile = createAssetFile();
    return currentAsset;
  }

  private File createAssetFile() {
    File tempFile = null;
    String tempFileName = null;
    InputStream is = null;
    FileOutputStream fos = null;

    try {
      tempFileName = StringUtils.rightPad(currentAsset.getFileName(), 5, "x");
      tempFile =
          File.createTempFile(
              tempFileName,
              null,
              Components.getComponent(FileSystemHelper.class).getTempDirectory());
      tempFile.deleteOnExit();
      fos = new FileOutputStream(tempFile);
      is = currentAsset.getContentStream();
      IOUtils.copyLarge(is, fos);
      fos.flush();
    } catch (Exception e) {
      LOG.warn("Could not create tmp file " + tempFileName, e);
    } finally {
      IOUtils.closeQuietly(is);
      IOUtils.closeQuietly(fos);
    }

    return tempFile;
  }

  private void setupSelectButton() {
    selectButton.addStyleName("magnoliabutton");
    selectButton.setCaption("Bild auswählen");
  }

  private void setupLinkLayout() {
    linkLayout.setSizeFull();
    linkLayout.addComponent(textField);
    linkLayout.setExpandRatio(textField, 1);
    linkLayout.setComponentAlignment(textField, Alignment.MIDDLE_LEFT);
    linkLayout.addComponent(selectButton);
    linkLayout.setExpandRatio(selectButton, 0);
    linkLayout.setComponentAlignment(selectButton, Alignment.MIDDLE_RIGHT);
  }

  @Override
  public void setPropertyDataSource(Property newDataSource) {
    super.setPropertyDataSource(newDataSource);
    currentValue = (ImgixAsset) newDataSource.getValue();
    textField.addValueChangeListener(this::refreshPreview);
    updateFields();
  }

  @Override
  public ImgixAsset getValue() {
    return currentValue;
  }

  @Override
  public void setValue(ImgixAsset newValue)
      throws ReadOnlyException, Converter.ConversionException {
    currentValue = newValue;
    updateFields();
  }

  private void updateFields() {
    if (currentValue != null) {
      final String identifier = currentValue.getIdentifier();

      if (StringUtils.isNotBlank(identifier)) {
        textField.setValue(converter.convertToPresentation(identifier, String.class, getLocale()));
      }

      final ImgixFocalPoint focalPoint = currentValue.getFocalPoint();

      if (focalPoint != null) {
        focalPointLabel.setValue(
            "<strong>Focal-Point:</strong> (X="
                + focalPoint.getX()
                + ", Y="
                + focalPoint.getY()
                + ")");
      }
    }
  }

  @Override
  public Class<? extends ImgixAsset> getType() {
    return ImgixAsset.class;
  }

  public Button getSelectButton() {
    return selectButton;
  }
}
