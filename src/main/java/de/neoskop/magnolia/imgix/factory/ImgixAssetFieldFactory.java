package de.neoskop.magnolia.imgix.factory;

import com.vaadin.v7.data.Item;
import com.vaadin.v7.ui.Field;
import de.neoskop.magnolia.imgix.definition.ImgixAssetFieldDefinition;
import de.neoskop.magnolia.imgix.field.ImgixAssetField;
import de.neoskop.magnolia.imgix.model.ImgixAsset;
import de.neoskop.magnolia.imgix.transformer.ImgixAssetTransformer;
import info.magnolia.dam.app.assets.field.translator.AssetCompositeIdKeyTranslator;
import info.magnolia.ui.api.app.AppController;
import info.magnolia.ui.api.app.ChooseDialogCallback;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.AbstractFieldFactory;
import info.magnolia.ui.form.field.transformer.Transformer;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemUtil;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 28.02.17
 */
public class ImgixAssetFieldFactory
    extends AbstractFieldFactory<ImgixAssetFieldDefinition, ImgixAsset> {

  private static final Logger LOG = LogManager.getLogger(ImgixAssetFieldFactory.class);
  private final UiContext uiContext;
  private final AppController appController;
  private ImgixAssetField assetField;

  @Inject
  public ImgixAssetFieldFactory(
      ImgixAssetFieldDefinition definition,
      Item relatedFieldItem,
      UiContext uiContext,
      AppController appController,
      I18NAuthoringSupport i18NAuthoringSupport) {
    super(definition, relatedFieldItem, uiContext, i18NAuthoringSupport);
    this.uiContext = uiContext;
    this.appController = appController;
  }

  @Override
  protected Field<ImgixAsset> createFieldComponent() {
    assetField = new ImgixAssetField(definition.getName());
    assetField
        .getSelectButton()
        .addClickListener(
            e ->
                appController.openChooseDialog(
                    "assets",
                    uiContext,
                    assetField.getValue() == null ? null : assetField.getValue().getIdentifier(),
                    new ChooseDialogCallback() {
                      @Override
                      public void onCancel() {}

                      @Override
                      public void onItemChosen(String actionName, Object itemId) {
                        String newValue = null;

                        if (itemId instanceof JcrItemId) {
                          String propertyName = definition.getName();

                          try {
                            javax.jcr.Item jcrItem = JcrItemUtil.getJcrItem((JcrItemId) itemId);

                            if (jcrItem.isNode()) {
                              final Node selected = (Node) jcrItem;
                              boolean isPropertyExisting =
                                  StringUtils.isNotBlank(propertyName)
                                      && selected.hasProperty(propertyName);
                              newValue =
                                  isPropertyExisting
                                      ? selected.getProperty(propertyName).getString()
                                      : selected.getPath();
                            }
                          } catch (RepositoryException e) {
                            LOG.error(
                                "Not able to access the configured property. Value will not be set.",
                                e);
                          }
                        } else {
                          newValue = String.valueOf(itemId);
                        }

                        final AssetCompositeIdKeyTranslator converter =
                            new AssetCompositeIdKeyTranslator();
                        converter.setWorkspaceName("dam");
                        final String uuid = converter.convertToModel(newValue, String.class, null);
                        assetField.setValue(new ImgixAsset(uuid));
                      }
                    }));

    return assetField;
  }

  @Override
  protected Transformer<?> initializeTransformer(Class<? extends Transformer<?>> transformerClass) {
    Transformer<?> transformer = new ImgixAssetTransformer(item, definition, ImgixAsset.class);
    transformer.setLocale(getLocale());
    return transformer;
  }
}
