# README

Allgemeine Beschreibung vom Modul **Imgix**.

# Abhängigkeiten

Bis Version 1.3.2:

- [Magnolia CMS][1] >= 5.3.7

Ab Version 1.4.0:

- [Magnolia CMS][1] >= 5.6.7

# Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-imgix</artifactId>
	<version>1.4.0</version>
</dependency>
```

[1]: https://www.magnolia-cms.com
